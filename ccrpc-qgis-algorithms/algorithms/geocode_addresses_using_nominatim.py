from .base import BaseAlgorithm
from qgis.core import QgsProcessingParameterString, \
    QgsProcessingParameterFeatureSource, QgsProcessingParameterField, \
    QgsSettings, QgsField, QgsProcessingParameterFeatureSink, QgsFeatureSink, \
    QgsFeature, QgsGeometry, QgsPointXY, \
    QgsProcessing, QgsWkbTypes, QgsCoordinateReferenceSystem, \
    QgsProcessingParameterCrs, QgsCoordinateTransform, QgsProject
from PyQt5.QtCore import QVariant
import json
import urllib.request
import urllib.parse


class GeocodeAddressesUsingNominatim(BaseAlgorithm):
    FOLDER = 'FOLDER'
    SOURCE = 'SOURCE'
    ADDRESS = 'ADDRESS'
    CITY = 'CITY'
    STATE = 'STATE'
    ZIP_CODE = 'ZIP_CODE'
    STATUS = 'STATUS'
    NOMINATIM_URL = 'NOMINATIM_URL'
    OUTPUT = 'OUTPUT'
    CRS = 'CRS'
    URL_KEY = 'ccrpc-qgis-algorithms/nominatim_url'

    GENERAL_HELP = \
        '''This algorithm geocodes street addresses using Nominatim.
        '''.replace('\n', '')
    ADDRESS_HELP = \
        '''Address Help:  There are two possible input format for the
        address parameter. Example 1: complete address containing in one single
        field ( 1001 W Main St, Chicago, IL 60608).  In this case, the address
        should use the address parameter and leave the other empty.
        '''.replace('\n', '')
    ADDRESS_EX_TWO_HELP = \
        '''Example 2: Each geographical unit is separated into a distinct field.
        The address is separated into fields: 1001 W Main St | Chicago | IL | 60608
         In this case, select the appropriate field in the parameter.
        '''.replace('\n', '') # noqa
    STATUS_FIELD_HELP = \
        '''
        Status Field Help:  The status field will determine whether the record
        will be passed through for geocoding.  If left empty, the geocoder will
        geocode all the record and create a new field named "geocode_status",
        the value will either be "Success" or "Failure".  If the status field
        is supplied to the geocoder, the records with the value of "Success" or
        "Skip" will not be geocoded.
        '''.replace('\n', '')
    NOMINATIM_HELP = \
        '''
        Nominatim Help:  The base URL for the Nominatim server. Default to
        OpenStreetMap: https://nominatim.openstreetmap.org/
        '''.replace('\n', '')
    COORDINATE_HELP = \
        '''
        Coordinate System Help: This optional parameter will transform the
        output data into the user-supplied coordinate system.  If left empty,
        the coordinate system will be transform following this order if they
        are available. Source Feature's CRS > Project's CRS > WGS 84
        '''.replace('\n', '')
    SETTING = QgsSettings()

    def name(self):
        return 'geocodeaddressesusingnominatim'

    def displayName(self):
        return self.tr('Geocode Addresses Using Nominatim')

    def group(self):
        return self.tr('Geocoding')

    def groupId(self):
        return 'geocoding'

    def shortHelpString(self):
        help = [
            self.GENERAL_HELP,
            self.ADDRESS_HELP,
            self.ADDRESS_EX_TWO_HELP,
            self.STATUS_FIELD_HELP,
            self.NOMINATIM_HELP,
            self.COORDINATE_HELP
        ]
        return self.tr('\n\n'.join(help))

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterFeatureSource(
            self.SOURCE,
            self.tr('Feature Source'),
            types=[QgsProcessing.TypeFile]))

        self.addParameter(QgsProcessingParameterField(
            self.ADDRESS,
            self.tr('Address Field'),
            parentLayerParameterName=self.SOURCE))

        self.addParameter(QgsProcessingParameterField(
            self.CITY,
            self.tr('City Field'),
            parentLayerParameterName=self.SOURCE,
            optional=True))

        self.addParameter(QgsProcessingParameterField(
            self.STATE,
            self.tr('State Field'),
            parentLayerParameterName=self.SOURCE,
            optional=True))

        self.addParameter(QgsProcessingParameterField(
            self.ZIP_CODE,
            self.tr('Zip Code Field'),
            parentLayerParameterName=self.SOURCE,
            optional=True))

        self.addParameter(QgsProcessingParameterField(
            self.STATUS,
            self.tr('Status Field'),
            parentLayerParameterName=self.SOURCE,
            optional=True))

        self.addParameter(QgsProcessingParameterString(
            self.NOMINATIM_URL,
            self.tr('Nominatim URL'),
            defaultValue=self._get_url()))

        self.addParameter(QgsProcessingParameterCrs(
            self.CRS,
            self.tr('Coordinate System'),
            optional=True))

        self.addParameter(QgsProcessingParameterFeatureSink(
            self.OUTPUT,
            self.tr('Output layer'),
            type=QgsProcessing.TypeVectorPoint))

    def processAlgorithm(self, parameters, context, feedback):
        in_source = self.parameterAsSource(parameters, self.SOURCE, context)
        address = self._pick(
                self.parameterAsFields(parameters, self.ADDRESS, context))
        city = self._pick(
                self.parameterAsFields(parameters, self.CITY, context))
        state = self._pick(
                self.parameterAsFields(parameters, self.STATE, context))
        zip_code = self._pick(
                self.parameterAsFields(parameters, self.ZIP_CODE, context))
        in_status_name = self._pick(
                self.parameterAsFields(parameters, self.STATUS, context))
        in_crs = self.parameterAsCrs(parameters, self.CRS, context)

        # select the correct out_crs, user selection > source feature crs >
        # project crs > wgs84
        if not in_crs.isValid():
            in_crs = in_source.sourceCrs()
        if not in_crs.isValid():
            proj = QgsProject.instance()
            in_crs = proj.crs()
        if not in_crs.isValid():
            QgsCoordinateReferenceSystem(4326)

        out_fields = in_source.fields()
        # Create status field if it doesn't exist
        if not in_status_name:
            new_status_name = 'geocode_status'
            status_type = QVariant.String
            status_field = QgsField(new_status_name, status_type)
            out_fields.append(status_field)
        sink, output_id = self.parameterAsSink(
                               parameters,
                               self.OUTPUT,
                               context,
                               out_fields,
                               QgsWkbTypes.Point,
                               in_crs)
        base_url = self.parameterAsString(parameters,
                                          self.NOMINATIM_URL,
                                          context)

        # Progress Bar
        total = 100.0 / in_source.featureCount() if \
            in_source.featureCount() else 0
        current = 0

        in_features = in_source.getFeatures()
        # Main loop - loop through the feature and process
        for feature in in_features:
            if feedback.isCanceled():
                break

            attributes = feature.attributes()
            out_feature = QgsFeature()
            # Geocode if status is not Skip or Success
            if self._get_attr(feature, in_status_name) != 'Skip' and \
               self._get_attr(feature, in_status_name) != 'Success':
                out_status, out_geom = self.geocode(
                            base_url,
                            self._get_attr(feature, address),
                            self._get_attr(feature, city),
                            self._get_attr(feature, state),
                            self._get_attr(feature, zip_code))
                # Transform the feature if needed
                if out_geom is not None:
                    out_geom = self._transform(out_geom, in_crs)
                # Update status field
                if in_status_name:
                    out_feature.setFields(feature.fields())
                    out_feature.setAttributes(attributes)
                    out_feature[in_status_name] = out_status
                else:
                    attributes.append(out_status)
                    out_feature.setAttributes(attributes)

                out_feature.setGeometry(out_geom)
            else:
                out_feature.setAttributes(attributes)

            sink.addFeature(out_feature, QgsFeatureSink.FastInsert)

            current += 1
            feedback.setProgress(int(current * total))

        # Save url setting
        self._store_setting(base_url)

        return {self.OUTPUT: sink}

    def geocode(self, url, address, city, state, zip):
        url = self._construct_url(url, address, city, state, zip)
        data = json.load(urllib.request.urlopen(url))
        status = self._generate_status(data)
        if data:
            lat = float(data[0]['lat'])
            long = float(data[0]['lon'])
            return [status, QgsGeometry.fromPointXY(QgsPointXY(long, lat))]
        else:
            return [status, None]

    def _transform(self, geom, to_crs, source_srid=4326):
        source_crs = QgsCoordinateReferenceSystem(source_srid)
        tr = QgsCoordinateTransform(source_crs, to_crs, QgsProject.instance())
        geom.transform(tr)
        return geom

    def _construct_url(self, base_url, address, city, state, zip):
        data = {'format': 'json',
                'limit': 1}
        if city or state or zip:
            data['street'] = address
            if city:
                data['city'] = city
            if state:
                data['state'] = state
            if zip:
                data['zip'] = zip
            return "{}search?{}".format(
                                base_url,
                                urllib.parse.urlencode(data))
        else:
            urllib.parse.quote(address)
            data['q'] = address
            return "{}search?{}".format(base_url, urllib.parse.urlencode(data))

    def _generate_status(self, data):
        if not data:
            return 'Failure'
        else:
            return 'Success'

    def _pick(self, input):
        if not input:
            return None
        else:
            return input[0]

    def _get_attr(self, feature, attr):
        if attr:
            return feature.attribute(attr)
        else:
            return None

    def _get_url(self):
        url = self.SETTING.value(self.URL_KEY, None)
        if url is None:
            url = 'https://nominatim.openstreetmap.org/'
        return url

    def _store_setting(self, url):
        s = QgsSettings()
        s.setValue(self.URL_KEY, url)
