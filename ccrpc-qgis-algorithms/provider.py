import os
from qgis.PyQt.QtGui import QIcon
from qgis.core import QgsProcessingProvider
from .algorithms import CopyNetworkAttributes, \
    CreateNetworkMatchTable, CreateProjectFolder, ExportArcGISAttachments, \
    GeocodeAddressesUsingNominatim

plugin_path = os.path.dirname(__file__)


class CCRPCAlgorithmProvider(QgsProcessingProvider):

    def __init__(self):
        super().__init__()

    def id(self):
        return 'ccrpc'

    def name(self):
        return 'CCRPC'

    def icon(self):
        return QIcon(self.svgIconPath())

    def svgIconPath(self):
        return os.path.join(plugin_path, 'images', f'{self.id()}.svg')

    def loadAlgorithms(self):
        algs = [
            CopyNetworkAttributes(),
            CreateNetworkMatchTable(),
            CreateProjectFolder(),
            ExportArcGISAttachments(),
            GeocodeAddressesUsingNominatim()
        ]
        for alg in algs:
            self.addAlgorithm(alg)

    def supportsNonFileBasedOutput(self):
        return True
